package rest;

import io.jsonwebtoken.Jwts;
import io.vertx.core.Vertx;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.web.RoutingContext;

import java.util.Optional;
import java.util.Random;

public class UserAuthenticator {
    public static JWTAuth provider(Vertx vertx) {
        return JWTAuth.create(vertx, new JWTAuthOptions()
                .addPubSecKey(new PubSecKeyOptions()
                        .setAlgorithm("HS256")
                        .setPublicKey("keyboard cat")
                        .setSymmetric(true)));
    }

    public static Optional<Integer> requireIdentity(RoutingContext context) {
        String jwt = context.request().getHeader("authorization");

        int id = 1 + new Random().nextInt(100);

        return Optional.ofNullable(id);
    }
}
