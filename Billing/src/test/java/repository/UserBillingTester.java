package repository;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Vertx;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(VertxUnitRunner.class)
public class UserBillingTester {
     private final String DB_COLLECTION = "test_calculations";
     private UsersRepository usersRepo;
     private MongoClient mongoClient;
    private Vertx vertx;

    @Before
    public void setUp(){
         this.vertx = Vertx.vertx();
         this.mongoClient = new MongoClientFactory().create(vertx, DB_COLLECTION);
         this.usersRepo = new UsersRepository(this.mongoClient);
     }

     @Test
    public void shouldUpdateUserBalance(TestContext context) {
         UsersRepository.User sevak = UsersRepository.User.create(2, 200);
         this.usersRepo.add(UsersRepository.User.create(1, 100))
                 .compose(objectId -> this.usersRepo.add(sevak))
                 .compose(objectId -> this.usersRepo.add(UsersRepository.User.create(3, 300)))
                 .compose(lastAdded -> this.usersRepo.get(1).setHandler(context.asyncAssertSuccess(res -> {
                     context.assertTrue(res.getBalance() == 100);
                 })))
                .compose(ignored -> {
                    sevak.updateBalance(120);
                    return this.usersRepo.updateUser(sevak);
                })
                .compose(updated -> this.usersRepo.get(sevak.getId())
                        .setHandler(context.asyncAssertSuccess(res -> context.assertTrue(res.getBalance() == 120)))
                )
                .setHandler(context.asyncAssertSuccess());
     }

    @Test
    public void populateRandomUsers(TestContext testContext) {
        UsersRepository prodRepository = new UsersRepository(new MongoClientFactory().create(this.vertx));
        MonitoringRepository monitoringRepository = new MonitoringRepository(new MongoClientFactory().create(this.vertx));

        List<UsersRepository.User> users = Arrays.asList(
                UsersRepository.User.create(1, 120),
                UsersRepository.User.create(2, 50),
                UsersRepository.User.create(3, 2200),
                UsersRepository.User.create(4, 25),
                UsersRepository.User.create(5, 78),
                UsersRepository.User.create(6, 900)
        );

        List<MonitoringRepository.InstanceLoad> instances = Arrays.asList(
                MonitoringRepository.InstanceLoad.create(1, 50),
                MonitoringRepository.InstanceLoad.create(2, 50),
                MonitoringRepository.InstanceLoad.create(3, 50)
        );

        CompositeFuture.all(users.stream().map(prodRepository::add).collect(Collectors.toList()))
                .compose(usersAdded -> CompositeFuture.all(instances.stream()
                        .map(monitoringRepository::add)
                        .collect(Collectors.toList())
                )
                .setHandler(testContext.asyncAssertSuccess()));
        ;
    }

}
