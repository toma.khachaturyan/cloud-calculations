package business;

import constants.OperationWeight;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import lombok.extern.slf4j.Slf4j;
import repository.MongoClientFactory;
import repository.UsersRepository;

@Slf4j
public class BillingRestApi extends AbstractVerticle {

    private  static  final int DEFAULT_PORT = 8081;
    private BillingService billingService;

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        log.info("Billing: HTTP Server Created");

        this.billingService = new BillingService(new MongoClientFactory().create(vertx));

        Router application = installRoutes();
        this.runServer(application).setHandler(startPromise);
    }

    private Future<Void> runServer(Router router) {
        Promise<Void> serverStarted = Promise.promise();

        vertx.createHttpServer()
                .requestHandler(router)
                .listen(DEFAULT_PORT, ar->{
                    if(ar.succeeded()){
                        log.info("Billing: HTTP Server Created");
                        serverStarted.complete();
                    } else {
                        log.error("failed to deploy billingRespApi", ar.cause());
                        serverStarted.fail(ar.cause());
                    }
                });

        return serverStarted.future();
    }

    private Router installRoutes() {
        Router application = Router.router(vertx);
        application.get("/test").handler(this::handleTest);
        application.post("/:userId/bill").handler(this::handleBilling);
        application.post("/:userId/topup")
                .consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(this::handleTopup);
        application.get("/:userId/balance").handler(this::handleBalance);
        return application;
    }

    private void handleBalance(RoutingContext context) {
        int userId = new Integer(context.pathParam("userId"));
        log.info("balance for user {}", userId);
        this.billingService.getUser(userId).setHandler(ar -> {
            if(ar.succeeded()) {
                context.response().setStatusCode(200).end(success(ar.result()).encode());
            } else {
                context.response().setStatusCode(500).end(internalServerError().encode());
            }
        });
    }

    private void handleTopup(RoutingContext context) {
        int userId = new Integer(context.pathParam("userId"));
        double increaseBy = new Double(context.getBodyAsJson().getDouble("increaseBy"));

        this.billingService.topup(userId, increaseBy).setHandler(ar -> {
            if(ar.succeeded()) {
                context.response().setStatusCode(201).end(success(ar.result()).encode());
            } else {
                context.response().setStatusCode(500).end(internalServerError().encode());
            }
        });
    }

    private void handleBilling(RoutingContext context) {
        int userId = new Integer(context.pathParam("userId"));
        int instanceId = new Integer(context.queryParams().get("instance_id"));
        OperationWeight operation = OperationWeight.valueOf(context.queryParams().get("operation"));

        this.billingService.bill(userId, instanceId, operation).setHandler(ar -> {
            if (ar.succeeded()) {
                context.response().setStatusCode(200).end(success(ar.result()).encode());
            } else {
                context.response().setStatusCode(500).end(internalServerError().encode());
            }
        });
    }

    private JsonObject internalServerError() {
        return new JsonObject().put("message", "internal server error");
    }

    private static JsonObject success() {
        return new JsonObject().put("message", "success");
    }

    private static JsonObject success(UsersRepository.User user) {
        return new JsonObject().put("message", "success").put("user_id", user.getId()).put("balance", user.getBalance());
    }

    private void handleTest(RoutingContext context) {
        context.response().setStatusCode(200).end("hey there");
    }
}
