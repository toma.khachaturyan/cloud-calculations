package business;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Slf4j
public class CalculationsHelper {
    public static List<Long> getPrimes(long a, long b) {
        return LongStream.rangeClosed(a, b)
                .filter(i->isPrime(i))
                .boxed()
                .collect(Collectors.toList());
    }

    public static boolean isPrime(long num) {
       return num > 1 && LongStream
                .rangeClosed(2, (long)Math.sqrt(num))
                .allMatch(i -> num % i != 0);
    }

    public static int fibonacci(int num) {
        assert (num >= 0);
        if (num <= 1) {
            return num;
        } else {
            return fibonacci(num-1) + fibonacci(num - 2);
        }
    }
}
