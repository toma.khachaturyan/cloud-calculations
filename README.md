#### Prerequisites

- minikube (recommended kubernetes version 1.15.7)
- kubectl 
- istio (recommended version 1.4.5)

#### Start minikube

- minikube config set kubernetes-version v1.15.7
- minikube start

#### Installing Istio

 - export PATH=$PWD/bin:$PATH
 - istioctl manifest apply --set profile=demo
 
enabling sidecar injection for the default namespace

- kubectl label namespace default istio-injection=enabled 

#### Running the Application

##### Creating mongoDB and RabbitMQ containers and volumes
- kubectl apply -f k8s/mongo_rabbitmq.yml
- kubectl apply -f k8s/calculations.yml
- kubectl apply -f k8s/billing.yml
- kubectl apply -f k8s/control-center.yml

##### Create ingress gateway

- kubectl apply -f istio/gateway.yml

##### Create virtual services

- kubectl apply -f istio/virtual_svc.yml

##### Exporting variables

- export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
- export INGRESS_HOST=$(minikube ip)
- echo INGRESS_HOST=$INGRESS_HOST, INGRESS_PORT=$INGRESS_PORT

##### Check availability
the application should be available then via [http://host:port] (for example [192.168.64.51:30080])

for checking control-center availability send GET request to [http://host:port/test]

##### Starting Grafana and Kiali

- istioctl dashboard kiali
- istioctl dashboard grafana

##### Fault Injectcion
###### injecting fault
- kubectl apply -f istio/fault_injection.yml
###### delete fault injection
- kubectl delete -f istio/fault_injection.yml

##### Timeouts
###### apply timeout on gateway
- kubectl apply -f istio/timeout.yml
###### delete timeout on gateway
- kubectl delete -f istio/timeout.yml

#### Rate Limiting (memquota)
###### apply rate limiting using memquota
- kubectl apply -f istio/rate-limiting-memquota.yml
###### delete rate limiting rules
- kubectl delete -f istio/rate-limiting-memquota.yml

#### Rate Limiting (redisquota)
###### start Redis server
- kubectl apply -f k8s/redis.yml

###### apply rate limiting using redisquota
- kubectl apply -f istio/rate-limiting-redisquota.yml

###### delete rate limiting using redisquota
- kubectl delete -f istio/rate-limiting-redisquota.yml
