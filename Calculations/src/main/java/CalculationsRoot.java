import business.CalculationsRestApi;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Promise;
import lombok.extern.slf4j.Slf4j;
import monitoring.OverflowMonitoring;

@Slf4j
public class CalculationsRoot extends AbstractVerticle {
    @Override
    public void start(Promise<Void> startPromise) throws Exception {

        log.info("CalculationsRoot Service started");
        // TODO move to separate method that returns a Future<Void>
        vertx.deployVerticle(CalculationsRestApi::new, new DeploymentOptions(), ar -> {
            if (ar.succeeded()) {
                vertx.deployVerticle(OverflowMonitoring::new, new DeploymentOptions(), monitoringAr -> {
                    if (ar.succeeded()) {
                        startPromise.complete();
                    } else {
                        startPromise.fail(monitoringAr.cause());
                    }
                });
            } else {
                startPromise.fail(ar.cause());
            }
        });
    }
}
