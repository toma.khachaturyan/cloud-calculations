import io.vertx.core.impl.launcher.VertxCommandLauncher;

import java.util.Arrays;

public class CalculationsLauncher extends VertxCommandLauncher {
    public static void main(String[] args) {
        new CalculationsLauncher().dispatch(Arrays.asList("run", CalculationsRoot.class.getCanonicalName()).toArray(new String[0]));
    }
}
