import io.vertx.core.impl.launcher.VertxCommandLauncher;

import java.util.Arrays;

public class BillingLauncher extends VertxCommandLauncher {
    public static void main(String[] args) {
        new BillingLauncher().dispatch(Arrays.asList("run", BillingRoot.class.getCanonicalName()).toArray(new String[0]));
    }
}
