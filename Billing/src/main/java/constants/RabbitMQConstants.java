package constants;

import lombok.Getter;

public enum RabbitMQConstants {
    EXCHANGE_NAME_OVERLOAD ("user"),
    EXCHANGE_TYPE ("direct"),
    ROUTING_KEY_OVERLOAD ("overload"),
    MESSAGE_BODY ("body"),
    QUEUE_NAME_JSON_KEY ("queue"),
    QUEUE_NAME ("overload_factor"),
    EVENT_BUS_ADDRESS ("user.login"),
    CONSUMER_BILLING ("billing");

    @Getter
    private String value;

    private RabbitMQConstants(String value){
        this.value = value;
    }
}