package repository;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class MonitoringRepository {
    private static final String PROD_DB_COLLECTION = "instances";

    private final MongoClient client;

    public Future<InstanceLoad> get(int id) {
        Promise<InstanceLoad> result = Promise.promise();

        JsonObject query = new JsonObject().put("id", id);

        this.client.findOne(PROD_DB_COLLECTION, query, null, ar -> {
            if (ar.succeeded()) {
                JsonObject raw = ar.result();

                result.complete(InstanceLoad.create(raw.getInteger("id"), raw.getDouble("load")));
            } else {
                result.fail(ar.cause());
            }
        });

        return result.future();
    }

    public Future<InstanceLoad> get(String objectId) {
        Promise<InstanceLoad> result = Promise.promise();

        JsonObject query = new JsonObject().put("_id", objectId);

        this.client.findOne(PROD_DB_COLLECTION, query, null, ar -> {
            if (ar.succeeded()) {
                JsonObject raw = ar.result();

                result.complete(InstanceLoad.create(raw.getInteger("id"), raw.getDouble("load")));
            } else {
                result.fail(ar.cause());
            }
        });

        return result.future();
    }

    public Future<Void> update(InstanceLoad newLoad) {
        Promise<Void> update = Promise.promise();
        JsonObject query = new JsonObject().put("id", newLoad.id);
        JsonObject replace = new JsonObject().put("id", newLoad.id).put("load", newLoad.load);

        this.client.findOneAndReplace(PROD_DB_COLLECTION, query, replace, ar -> {
            if (ar.succeeded()) {
                update.complete();
            } else {
                update.fail(ar.cause());
            }
        });
        return update.future();
    }


    public Future<String> add(@NonNull InstanceLoad instanceLoad) {
        Promise<String> objectIdResult = Promise.promise();

        JsonObject document = new JsonObject().put("id", instanceLoad.id).put("load", instanceLoad.load);

        this.client.insert(PROD_DB_COLLECTION, document, objectIdResult);
        return objectIdResult.future();
    }

    @Value
    public static class InstanceLoad {
        private final int id;
        private final double load;

        InstanceLoad(int id, double load) {
            if (load > 100 || load < 0) {
                throw new IllegalArgumentException("load must be between 0 and 100");
            }

            this.id = id;
            this.load = load;
        }

        public static InstanceLoad create(int id, double load) {
            return new InstanceLoad(id, load);
        }
    }
}
