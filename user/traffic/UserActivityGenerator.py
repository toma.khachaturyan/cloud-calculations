import json

import numpy
import requests
from flask import Flask
from numpy.core._multiarray_umath import ndarray

app = Flask(__name__)

# control-center gateway url
minikube = "http://192.168.64.61:30725"



def post_addition():
    url = minikube
    headers = {'Content-Type': 'application/json'}
    url += "/add"
    nums: ndarray = numpy.asarray(numpy.random.uniform(0.0, 1.7*(10^308), size=(1,5))[0])
    j = {"nums": nums.tolist()}
    res = requests.post(url, json.dumps(j), headers=headers)
    print(res.status_code)


def post_subtraction():
    url = minikube
    headers = {'Content-Type': 'application/json'}
    url += "/subtract"
    nums: ndarray = numpy.asarray(numpy.random.uniform(0.0, 1.7 * (10 ^ 308), size=(1, 5))[0])
    j = {"nums": nums.tolist()}
    res = requests.post(url, json.dumps(j), headers=headers)
    print(res.status_code)


def post_multiplication():
    url = minikube
    headers = {'Content-Type': 'application/json'}
    url += "/multiply"
    nums: ndarray = numpy.asarray(numpy.random.uniform(0.0, 1.7 * (10 ^ 308), size=(1, 5))[0])
    j = {"nums": nums.tolist()}
    res = requests.post(url, json.dumps(j), headers=headers)
    print(res.status_code)


def post_division():
    url = minikube
    headers = {'Content-Type': 'application/json'}
    url += "/divide"
    nums: ndarray = numpy.asarray(numpy.random.uniform(0.0, 1.7 * (10 ^ 308), size=(1, 5))[0])
    j = {"nums": nums.tolist()}
    res = requests.post(url, json.dumps(j), headers=headers)
    print(res.status_code)


def post_primes():
    url = minikube
    headers = {'Content-Type': 'application/json'}
    url += "/primes"
    a = numpy.asarray(numpy.random.randint(low=0, high=1000, size=1)[0])
    b = numpy.asarray(numpy.random.randint(low=0, high=1000, size=1)[0])
    j = {"from": a.tolist(),
         "to": b.tolist() }
    res = requests.post(url, json.dumps(j), headers=headers)
    print(res.status_code)

def post_fibonacci():
    url = minikube
    headers = {"Content-Type": 'application/json'}
    url += "/fibonacci"
    num = numpy.asarray(numpy.random.randint(low=0, high=50, size=1)[0])
    j = {"num": num.tolist()}
    res = requests.post(url, json.dumps(j), headers=headers)
    print(res.status_code)

def post_fibonacci_bad_request():
    url = minikube
    headers = {"Content-Type": 'application/json'}
    url += "/fibonacci"
    num = numpy.asarray(numpy.random.randint(low=49, high=51, size=1)[0])
    j = {"num": num.tolist()}
    res = requests.post(url, json.dumps(j), headers=headers)
    print(res.status_code)

if __name__ == "__main__":


    while True:
        post_addition()
        post_division()
        post_subtraction()
        post_multiplication()
        post_primes()
        post_fibonacci()
