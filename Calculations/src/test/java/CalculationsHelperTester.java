import business.CalculationsHelper;
import org.junit.Test;

import static org.junit.Assert.*;


public class CalculationsHelperTester {

    @Test
    public void testIsPrime() {
        int nonPrime = 9;
        int prime = 13;

        assertTrue(CalculationsHelper.isPrime(prime));
        assertFalse(CalculationsHelper.isPrime(nonPrime));

        CalculationsHelper
                .getPrimes(2, 20).stream()
                .forEach(i -> assertTrue(CalculationsHelper.isPrime(i)));

        assertEquals(34, CalculationsHelper.fibonacci(9));
    }
}
