import business.BillingRestApi;
import constants.RabbitMQConstants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.RabbitMQConsumer;
import io.vertx.rabbitmq.RabbitMQMessage;
import io.vertx.rabbitmq.RabbitMQOptions;
import lombok.extern.slf4j.Slf4j;
import repository.MongoClientFactory;
import repository.MonitoringRepository;

import java.util.Objects;
import java.util.Optional;

@Slf4j
public class BillingRoot extends AbstractVerticle {
    private RabbitMQClient rabbitMQ;
    private MonitoringRepository monitoringRepository;


    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        String queue = Optional.ofNullable(System.getenv("QUEUE")).orElse(RabbitMQConstants.QUEUE_NAME.getValue());

        createRabbitMqClient().setHandler(rabbitConsumerCreated -> this.createRabbitConsumer(queue));
        createMonitoringRepository();

        log.info("started billing service");
        vertx.deployVerticle(BillingRestApi::new, new DeploymentOptions(), ar ->{
            if(ar.succeeded()){
                startPromise.complete();
            } else {
                startPromise.fail(ar.cause());
            }
        });


    }

    private void createMonitoringRepository() {
        MongoClient mongoClient = new MongoClientFactory().create(vertx);
        this.monitoringRepository = new MonitoringRepository(mongoClient);

    }

    private Future<Void> createRabbitConsumer(String queue) {

        if (Objects.isNull(this.rabbitMQ)) {
            throw new IllegalStateException("Rabbit mq must be started");
        }
        Promise<Void> whenCreated = Promise.promise();

        this.rabbitMQ.basicConsumer(queue, ar -> {
            if (ar.succeeded()) {
                log.info("RabbitMQ Consumer created");
                RabbitMQConsumer consumer = ar.result();
                consumer.handler(this::handleMessageFromQueue);
                whenCreated.complete();
            } else {
                whenCreated.fail(ar.cause());
            }
        });
        return whenCreated.future();
    }

    private Future<Void> createRabbitMqClient() {
        Promise<Void> whenCreated = Promise.promise();
        RabbitMQOptions config = new RabbitMQOptions()
                .setUser("guest")
                .setPassword("guest")
                .setHost("rabbitmq.default.svc.cluster.local")
                .setPort(5672)
                .setVirtualHost("/")
                .setConnectionTimeout(6000) // in milliseconds
                .setRequestedHeartbeat(60) // in seconds
                .setHandshakeTimeout(6000) // in milliseconds
                .setRequestedChannelMax(7)
                .setNetworkRecoveryInterval(500) // in milliseconds
                .setAutomaticRecoveryEnabled(true);

        this.rabbitMQ = RabbitMQClient.create(this.vertx, config);
        this.rabbitMQ.start(whenCreated);

        return whenCreated.future();
    }

    private Future<Void> handleMessageFromQueue(RabbitMQMessage rabbitMQMessage) {
        Promise udpatedLoad = Promise.promise();
        log.info("received this message from queueue {}", rabbitMQMessage.body());
        JsonObject messageBody = rabbitMQMessage.body().toJsonObject();
        int instanceId = messageBody.getInteger("instance");
        double load = messageBody.getDouble("overflow");
        this.monitoringRepository.update(MonitoringRepository.InstanceLoad.create(instanceId, load));

        return udpatedLoad.future();
    }
}
