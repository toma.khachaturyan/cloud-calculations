import io.vertx.core.impl.launcher.VertxCommandLauncher;

import java.util.Arrays;

public class ControlCenterLauncher extends VertxCommandLauncher {
    public static void main(String[] args) {
        new ControlCenterLauncher().dispatch(Arrays.asList("run", Root.class.getCanonicalName()).toArray(new String[0]));
    }
}
