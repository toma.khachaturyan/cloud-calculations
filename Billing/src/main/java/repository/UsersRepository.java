package repository;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import lombok.*;
import lombok.extern.slf4j.Slf4j;


@RequiredArgsConstructor
@Slf4j
public class UsersRepository {
    private static final String PROD_DB_COLLECTION = "users";
    private final MongoClient client;

    public Future<User> get(int id) {
        Promise<User> result = Promise.promise();
        JsonObject query = new JsonObject().put("id", id);
        this.client.findOne(PROD_DB_COLLECTION, query, null, ar -> {
            if (ar.succeeded()) {
                JsonObject raw = ar.result();

                result.complete(User.create(raw.getInteger("id"), raw.getDouble("balance")));
            } else {
                result.fail(ar.cause());
            }
        });

        return result.future();
    }
    public Future<User> get(String id) {
        Promise<User> result = Promise.promise();
        JsonObject query = new JsonObject().put("_id", id);
        this.client.findOne(PROD_DB_COLLECTION, query, null, ar -> {
            if (ar.succeeded()) {
                JsonObject raw = ar.result();

                result.complete(User.create(raw.getInteger("id"), raw.getDouble("balance")));
            } else {
                result.fail(ar.cause());
            }
        });

        return result.future();
    }

    public Future<String> add(@NonNull User user) {
        Promise<String> objectIdResult = Promise.promise();
        JsonObject document = new JsonObject().put("id", user.id).put("balance", user.balance);
        this.client.insert(PROD_DB_COLLECTION, document, objectIdResult);

        return objectIdResult.future();
    }



    public Future<Void> updateUser(User user){
        Promise<Void> update = Promise.promise();
        JsonObject query = new JsonObject().put("id", user.id);
        JsonObject replace = new JsonObject().put("id", user.id).put("balance", user.balance);

        this.client.findOneAndReplace(PROD_DB_COLLECTION, query, replace, ar -> {
            if (ar.succeeded()) {
                update.complete();
            } else {
                update.fail(ar.cause());
            }
        });
        return update.future();
    };

    @AllArgsConstructor
    public static class User {
        @Getter private final int id;
        @Getter private double balance;

        User(int id, int balance) {
            this.id = id;
            this.balance = balance;
        }

        public static User create(int id, double balance) {
            return new User(id, balance);
        }

        public void updateBalance(double balance) {
            this.balance = balance;
        }
    }
}
