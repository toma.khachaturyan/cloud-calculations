package rest;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.ext.web.handler.BodyHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class ControlCenterRestApi extends AbstractVerticle {
    private  static  final int DEFAULT_PORT = 8082;
    private WebClient webclient;
    private JWTAuth provider;

    @Override
    public void start(Promise<Void> startPromise) {
        this.webclient = WebClient.create(vertx);

        Router application = installRoutes();
        this.provider = UserAuthenticator.provider(vertx);

        this.runServer(application).setHandler(startPromise);
    }

    private Future<Void> runServer(Router router) {
        Promise<Void> serverStarted = Promise.promise();

        vertx.createHttpServer()
                .requestHandler(router)
                .listen(DEFAULT_PORT, ar->{
                    if(ar.succeeded()){
                        log.info("Control center: HTTP Server Created");
                        serverStarted.complete();
                    } else {
                        log.error("failed to deploy control center rest api", ar.cause());
                        serverStarted.fail(ar.cause());
                    }
                });

        return serverStarted.future();
    }

    private Router installRoutes() {
        Router application = Router.router(vertx);
        application.get("/test").handler(this::handleTest);
        application.post("/add")
                .consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(this::handleAddition);

        application.post("/subtract")
                .consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(this::handleSubtraction);

        application.post("/multiply").consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(this::handleMultiplication);

        application.post("/divide").consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(this::handleDivision);

        application.post("/primes").consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(this::handlePrimes);

        application.post("/fibonacci").consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(this::handleFibonacci);
        application.post("/topup").consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(this::handleTopup);
        application.get("/balance").handler(this::handleBalance);


        return application;
    }

    private void handleTopup(RoutingContext context) {
        UserAuthenticator.requireIdentity(context).ifPresent(userId -> {
            log.info("topup balance for user {}, balance + {}", userId, context.getBodyAsJson().getDouble("increaseBy"));
            Promise<HttpResponse<JsonObject>> user = Promise.promise();

            this.webclient.post(8081, "billing.default.svc.cluster.local", "/" + userId + "/topup")
//            this.webclient.post(8081, "localhost", "/" + userId + "/topup")
                    .as(BodyCodec.jsonObject())
                    .sendJsonObject(context.getBodyAsJson(), user);
            user.future().compose(resp -> filterStatusCode(resp, 201))
            .setHandler(json -> {
                if(json.succeeded()) {
                    context.response().setStatusCode(201).end(json.result().encode());
                } else {
                    context.response().setStatusCode(500).end(internalServerError().encode());
                }
            });
        });

    }

    private void handleBalance(RoutingContext context) {

        UserAuthenticator.requireIdentity(context).ifPresent(userId -> {
            log.info("requesting balance for user with userId {}", userId);

            this.webclient.get(8081, "billing.default.svc.cluster.local", "/" + userId + "/balance")
//            this.webclient.get(8081, "localhost", "/" + userId + "/balance")
                    .send(ar -> {
                        if (ar.succeeded()) {
                            context.response().setStatusCode(200).end(ar.result().body());
                        } else {
                            context.response().setStatusCode(500).end(internalServerError().encode());
                        }
                    });
        });
    }


    private void handleSubtraction(RoutingContext context) {
        UserAuthenticator.requireIdentity(context).ifPresent(userId -> {
            log.info("handling subtraction for {}", userId);
            Promise<HttpResponse<JsonObject>> calculation = Promise.promise();

//            this.webclient.post(8080, "localhost", "/subtract")
            this.webclient.post(8080, "calculations.default.svc.cluster.local", "/subtract")
                    .as(BodyCodec.jsonObject())
                    .sendJsonObject(context.getBodyAsJson(), calculation);

            AtomicReference<Double> calculationResult = new AtomicReference<>((double) 0);

            calculation.future()
                    .compose(resp -> filterStatusCode(resp, 201))
                    .compose(json -> {
                        Promise<HttpResponse<JsonObject>> bill = Promise.promise();

                        calculationResult.set(json.getDouble("result"));

//                        this.webclient.post(8081, "localhost", "/" + userId + "/bill")
                        this.webclient.post(8081, "billing.default.svc.cluster.local", "/" + userId + "/bill")
                                .as(BodyCodec.jsonObject())
                                .addQueryParam("operation", "SUBTRACTION")
                                .addQueryParam("instance_id", json.getInteger("instance").toString())
                                .send(bill);

                        return bill.future();
                    })
                    .compose(billResponse -> filterStatusCode(billResponse, 200))
                    .setHandler(ar -> {
                        if (ar.failed()) {
                            log.error("failed request for subtraction", ar.cause());

                            if (!context.response().ended()) {
                                context.response().setStatusCode(500).end(internalServerError().encode());
                            }
                        } else {
                            String response = success(calculationResult.get(), ar.result().getDouble("balance")).encode();
                            context.response().setStatusCode(200).end(response);
                        }
                    });
        });

    }

    private void handleMultiplication(RoutingContext context) {
        UserAuthenticator.requireIdentity(context).ifPresent(userId -> {
            log.info("handling multiplication for {}", userId);
            Promise<HttpResponse<JsonObject>> calculation = Promise.promise();

//            this.webclient.post(8080, "localhost", "/multiply")
            this.webclient.post(8080, "calculations.default.svc.cluster.local", "/multiply")
                    .as(BodyCodec.jsonObject())
                    .sendJsonObject(context.getBodyAsJson(), calculation);

            AtomicReference<Double> calculationResult = new AtomicReference<>((double) 0);

            calculation.future()
                    .compose(resp -> filterStatusCode(resp, 201))
                    .compose(json -> {
                        Promise<HttpResponse<JsonObject>> bill = Promise.promise();

                        calculationResult.set(json.getDouble("result"));

//                        this.webclient.post(8081, "localhost", "/" + userId + "/bill")
                        this.webclient.post(8081, "billing.default.svc.cluster.local", "/" + userId + "/bill")
                                .as(BodyCodec.jsonObject())
                                .addQueryParam("operation", "MULTIPLICATION")
                                .addQueryParam("instance_id", json.getInteger("instance").toString())
                                .send(bill);

                        return bill.future();
                    })
                    .compose(billResponse -> filterStatusCode(billResponse, 200))
                    .setHandler(ar -> {
                        if (ar.failed()) {
                            log.error("failed request for multiplication", ar.cause());

                            if (!context.response().ended()) {
                                context.response().setStatusCode(500).end(internalServerError().encode());
                            }
                        } else {
                            String response = success(calculationResult.get(), ar.result().getDouble("balance")).encode();
                            context.response().setStatusCode(200).end(response);
                        }
                    });
        });

    }

    private void handleDivision(RoutingContext context) {
        UserAuthenticator.requireIdentity(context).ifPresent(userId -> {
            log.info("handling division for {}", userId);
            Promise<HttpResponse<JsonObject>> calculation = Promise.promise();

//            this.webclient.post(8080, "localhost", "/divide")
            this.webclient.post(8080, "calculations.default.svc.cluster.local", "/divide")
                    .as(BodyCodec.jsonObject())
                    .sendJsonObject(context.getBodyAsJson(), calculation);

            AtomicReference<Double> calculationResult = new AtomicReference<>((double) 0);

            calculation.future()
                    .compose(resp -> filterStatusCode(resp, 201))
                    .compose(json -> {
                        Promise<HttpResponse<JsonObject>> bill = Promise.promise();

                        calculationResult.set(json.getDouble("result"));

//                        this.webclient.post(8081, "localhost", "/" + userId + "/bill")
                        this.webclient.post(8081, "billing.default.svc.cluster.local", "/" + userId + "/bill")
                                .as(BodyCodec.jsonObject())
                                .addQueryParam("operation", "DIVISION")
                                .addQueryParam("instance_id", json.getInteger("instance").toString())
                                .send(bill);

                        return bill.future();
                    })
                    .compose(billResponse -> filterStatusCode(billResponse, 200))
                    .setHandler(ar -> {
                        if (ar.failed()) {
                            log.error("failed request for division", ar.cause());

                            if (!context.response().ended()) {
                                context.response().setStatusCode(500).end(internalServerError().encode());
                            }
                        } else {
                            String response = success(calculationResult.get(), ar.result().getDouble("balance")).encode();
                            context.response().setStatusCode(200).end(response);
                        }
                    });
        });

    }

    private void handlePrimes(RoutingContext context) {
        UserAuthenticator.requireIdentity(context).ifPresent(userId -> {
            log.info("handling primes for {}", userId);
            Promise<HttpResponse<JsonObject>> calculation = Promise.promise();

//            this.webclient.post(8080, "localhost", "/primes")
            this.webclient.post(8080, "calculations.default.svc.cluster.local", "/primes")
                    .as(BodyCodec.jsonObject())
                    .sendJsonObject(context.getBodyAsJson(), calculation);

            AtomicReference<JsonArray> calculationResult = new AtomicReference<>();

            calculation.future()
                    .compose(resp -> filterStatusCode(resp, 201))
                    .compose(json -> {
                        Promise<HttpResponse<JsonObject>> bill = Promise.promise();

                        calculationResult.set(json.getJsonArray("result"));

//                        this.webclient.post(8081, "localhost", "/" + userId + "/bill")
                        this.webclient.post(8081, "billing.default.svc.cluster.local", "/" + userId + "/bill")
                                .as(BodyCodec.jsonObject())
                                .addQueryParam("operation", "PRIMES")
                                .addQueryParam("instance_id", json.getInteger("instance").toString())
                                .send(bill);

                        return bill.future();
                    })
                    .compose(billResponse -> filterStatusCode(billResponse, 200))
                    .setHandler(ar -> {
                        if (ar.failed()) {
                            log.error("failed request for primes", ar.cause());

                            if (!context.response().ended()) {
                                context.response().setStatusCode(500).end(internalServerError().encode());
                            }
                        } else {
                            String response = success(calculationResult.get(), ar.result().getDouble("balance")).encode();
                            context.response().setStatusCode(200).end(response);
                        }
                    });
        });


    }

    private void handleFibonacci(RoutingContext context) {
        UserAuthenticator.requireIdentity(context).ifPresent(userId -> {
            log.info("handling fibonacci for {}", userId);
            Promise<HttpResponse<JsonObject>> calculation = Promise.promise();

//            this.webclient.post(8080, "localhost", "/fibonacci")
            this.webclient.post(8080, "calculations.default.svc.cluster.local", "/fibonacci")
                    .as(BodyCodec.jsonObject())
                    .sendJsonObject(context.getBodyAsJson(), calculation);

            AtomicReference<Double> calculationResult = new AtomicReference<>((double) 0);

            calculation.future()
                    .compose(resp -> filterStatusCode(resp, 201))
                    .compose(json -> {
                        Promise<HttpResponse<JsonObject>> bill = Promise.promise();

                        calculationResult.set(json.getDouble("result"));

//                        this.webclient.post(8081, "localhost", "/" + userId + "/bill")
                        this.webclient.post(8081, "billing.default.svc.cluster.local", "/" + userId + "/bill")
                                .as(BodyCodec.jsonObject())
                                .addQueryParam("operation", "FIBONACCI")
                                .addQueryParam("instance_id", json.getInteger("instance").toString())
                                .send(bill);

                        return bill.future();
                    })
                    .compose(billResponse -> filterStatusCode(billResponse, 200))
                    .setHandler(ar -> {
                        if (ar.failed()) {
                            log.error("failed request for fibonacci", ar.cause());

                            if (!context.response().ended()) {
                                context.response().setStatusCode(500).end(internalServerError().encode());
                            }
                        } else {
                            String response = success(calculationResult.get(), ar.result().getDouble("balance")).encode();
                            context.response().setStatusCode(200).end(response);
                        }
                    });
        });


    }

    private void handleAddition(RoutingContext context) {
        UserAuthenticator.requireIdentity(context).ifPresent(userId -> {
            log.info("handling addition for {}", userId);
            Promise<HttpResponse<JsonObject>> calculation = Promise.promise();

//            this.webclient.post(8080, "localhost", "/add")
            this.webclient.post(8080, "calculations.default.svc.cluster.local", "/add")
                    .as(BodyCodec.jsonObject())
                    .sendJsonObject(context.getBodyAsJson(), calculation);

            AtomicReference<Double> calculationResult = new AtomicReference<>((double) 0);

            calculation.future()
                .compose(resp -> filterStatusCode(resp, 201))
                .compose(json -> {
                    Promise<HttpResponse<JsonObject>> bill = Promise.promise();

                    calculationResult.set(json.getDouble("result"));

//                    this.webclient.post(8081, "localhost", "/" + userId + "/bill")
                    this.webclient.post(8081, "billing.default.svc.cluster.local", "/" + userId + "/bill")
                            .as(BodyCodec.jsonObject())
                            .addQueryParam("operation", "ADDITION")
                            .addQueryParam("instance_id", json.getInteger("instance").toString())
                            .send(bill);

                    return bill.future();
                })
                .compose(billResponse -> filterStatusCode(billResponse, 200))
                .setHandler(ar -> {
                    if (ar.failed()) {
                        log.error("failed request for addition", ar.cause());

                        if (!context.response().ended()) {
                            context.response().setStatusCode(500).end(internalServerError().encode());
                        }
                    } else {
                        String response = success(calculationResult.get(), ar.result().getDouble("balance")).encode();
                        context.response().setStatusCode(200).end(response);
                    }
                });
        });

    }

    private Future<JsonObject> filterStatusCode(HttpResponse<JsonObject> resp, int expectedCode) {
        log.info("received response {}", resp.body());
        return resp.statusCode() == expectedCode
                ? Future.succeededFuture(resp.body())
                : Future.failedFuture("request failed with status code " + resp.statusCode());
    }

    private JsonObject internalServerError() {
        return new JsonObject().put("message", "internal server error");
    }

    private static JsonObject success(double result, double balance) {
        return new JsonObject().put("result", result).put("balance", balance);
    }
    private static JsonObject success(JsonArray result, double balance) {
        return new JsonObject().put("result", result).put("balance", balance);
    }
    private static JsonObject userBalance (int id, double balance){
        return new JsonObject().put("userId", id).put("balance", balance);
    }

    private void handleTest(RoutingContext context) {
        context.response().setStatusCode(200).end("hey there");
    }
}
