package business;

import constants.OperationWeight;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.TimeoutHandler;
import lombok.extern.slf4j.Slf4j;
import monitoring.OverflowMonitoring;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
public class CalculationsRestApi extends AbstractVerticle {
    private static final int instance = Optional.ofNullable(System.getenv("INSTANCE"))
            .map(String::hashCode)
            .orElse(1);

    private static final int DEFAULT_PORT = 8080;
    private WorkerExecutor workerExecutor;

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        log.info("CalculationsRestApi started with instance id {}", instance);

        Router application = installRoutes();

        runServer(startPromise, application);
        this.workerExecutor = vertx.createSharedWorkerExecutor("calculations-worker");
    }

    private void runServer(Promise<Void> serverStarted, Router application) {
        vertx.createHttpServer()
                .requestHandler(application)
                .listen(DEFAULT_PORT, asyncResult -> {
                    if (asyncResult.succeeded()) {
                        log.info("server created");
                        serverStarted.complete();
                    } else {
                        log.error("failed to deploy http", asyncResult.cause());
                        serverStarted.fail(asyncResult.cause());
                    }
                });
    }

    private Router installRoutes() {
        Router application = Router.router(vertx);
        application.get("/hey").handler(this::handleHello);
        application.post("/add")
                .consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(context -> captureOperation(context, OperationWeight.ADDITION))
                .handler(this::handleAddition);

        application.post("/subtract")
                .consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(context -> captureOperation(context, OperationWeight.SUBTRACTION))
                .handler(this::handleSubtraction);

        application.post("/multiply")
                .consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(context -> captureOperation(context, OperationWeight.MULTIPLICATION))
                .handler(this::handleMultiplication);

        application.post("/divide")
                .consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(context -> captureOperation(context, OperationWeight.DIVISION))
                .handler(this::handleDivision);

        application.post("/primes")
                .consumes("application/json")
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(context -> captureOperation(context, OperationWeight.PRIMES))
                .handler(this::handlePrimes);

        application.post("/fibonacci")
                .consumes("application/json")
                .handler(TimeoutHandler.create(Duration.ofSeconds(10).toMillis()))
                .handler(BodyHandler.create(true).setBodyLimit(-1))
                .handler(context -> captureOperation(context, OperationWeight.FIBONACCI))
                .handler(this::handleFibonacci);
        return application;
    }

    private void captureOperation(RoutingContext context, OperationWeight operationWeight) {
        this.sendToMonitoring(operationWeight)
                .compose(ignored -> {
                    context.next();
                    return Future.succeededFuture();
                })
                .recover(err -> {
                    log.error("failed to transfer", err);
                    return Future.failedFuture(err);
                });
    }

    private Future<Void> sendToMonitoring(OperationWeight operationWeight) {
        return this.sendToMonitoring(operationWeight, false);
    }

    private Future<Void> sendToMonitoring(OperationWeight operation, boolean isCompleted) {
        JsonObject message = new JsonObject()
                .put("operation", operation.name())
                .put("weight", operation.getWeight())
                .put("completed", isCompleted);

        vertx.eventBus().send(OverflowMonitoring.ADDRESS, message);

        return Future.succeededFuture();
    }

    private void handleFibonacci(RoutingContext context) {
        log.info("Instance {} : Fibonacci", instance);
        int num = context.getBodyAsJson().getInteger("num");

        this.calculateFibonacci(num)
                .compose(fibonacci -> {
                    if (context.response().ended()) {
                        return Future.failedFuture("internal server error");
                    } else {
                        context.response()
                                .setStatusCode(201)
                                .end(success(fibonacci).encode());

                        return Future.succeededFuture();
                    }
                })
                .recover(err -> {
                    log.error("failed to calculate fibonacci", err.getCause());
                    if (!context.response().ended()) {
                        context.response()
                                .setStatusCode(500)
                                .end(new JsonObject().put("message", "internal server error").encode());
                    }

                    return Future.succeededFuture();
                });
    }

    private Future<Integer> calculateFibonacci(int inputNumber) {
        Promise<Integer> whenCalculated = Promise.promise();

        workerExecutor.executeBlocking(calculated -> calculated.complete(CalculationsHelper.fibonacci(inputNumber)), whenCalculated);

        return whenCalculated.future();
    }

    private void handlePrimes(RoutingContext context) {
        log.info("Instance {} : primes", instance);

        long from = context.getBodyAsJson().getLong("from");
        long to = context.getBodyAsJson().getLong("to");
        List<Long> result = CalculationsHelper.getPrimes(from, to);

        context.response()
                .setStatusCode(201)
                .end(success(result).encode());
    }

    private void handleDivision(RoutingContext context) {
        log.info("Instance {} : division", instance);
        Double result = this.numbersFromRequest(context)
                .stream()
                .reduce(new Double("1"), (div, current) -> div = div/current);
        context.response()
                .setStatusCode(201)
                .end(success(result).encode());
    }

    private void handleMultiplication(RoutingContext context) {
        log.info("Instance {} : multiplication", instance);
        Double result = this.numbersFromRequest(context)
                .stream()
                .reduce(new Double("1"), (mult, current) -> mult = mult * current);
        context.response()
                .setStatusCode(201)
                .end(success(result).encode());
    }


    private void handleSubtraction(RoutingContext context) {
        log.info("Instance {} : subtraction", instance);
        Double result = this.numbersFromRequest(context)
                .stream()
                .reduce(new Double("0"), (sub, current) -> sub = sub - current);

        context.response()
                .setStatusCode(201)
                .end(success(result).encode());

        // Marks operation as completed after 3 seconds
        vertx.setTimer(3000, timerId -> this.sendToMonitoring(OperationWeight.SUBTRACTION, true));
    }

    private void handleAddition(RoutingContext context) {
        log.info("Instance {} : addition", instance);
        Double result = this.numbersFromRequest(context)
                .stream()
                .reduce(new Double("0"), (agg, current) -> agg = agg + current);

        context.response()
                .setStatusCode(201)
                .end(success(result).encode());

        vertx.setTimer(3000, timerId -> this.sendToMonitoring(OperationWeight.ADDITION, true));
    }

    private JsonObject success(Double result) {
        return new JsonObject().put("result", result).put("instance", instance);
    }

    private JsonObject success(Integer result) {
        return new JsonObject().put("result", result).put("instance", instance);
    }

    private JsonObject success(List<Long> result) {
        return new JsonObject().put("result", result).put("instance", instance);
    }


    private void handleHello(RoutingContext context) {
        log.info("test request received");
        context.response().setStatusCode(200).end("ho");
    }


    private List<Double> numbersFromRequest(RoutingContext context) {
        return StreamSupport.stream(context.getBodyAsJson().getJsonArray("nums").spliterator(), false)
                .map(raw -> new Double(raw.toString()))
                .collect(Collectors.toList());
    }

}
