package repository;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import lombok.NonNull;


public class MongoClientFactory {

    private final String DEFAULT_URI = "mongodb://mongodb.default.svc.cluster.local:27017";
    private final String DEFAULT_DB = "calculations";

    public MongoClient create(Vertx vertx) {
        return MongoClient.createShared(vertx, configureConnection(DEFAULT_DB), "ProductionPool");
    }

    /**
     * Used for test collections
     */
    public MongoClient create(Vertx vertx, @NonNull String overrideCollection) {
        return MongoClient.createShared(vertx, configureConnection(overrideCollection), "TestPool");
    }

    private JsonObject configureConnection(@NonNull String collection) {

        return new JsonObject()
                .put("connection_string", DEFAULT_URI)
                .put("db_name", collection);
    }
}
