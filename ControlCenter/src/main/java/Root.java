import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import rest.ControlCenterRestApi;

public class Root extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        this.deployRest().setHandler(startPromise);
    }

    private Future<Void> deployRest() {
        Promise<String> whenDeployed = Promise.promise();
        this.vertx.deployVerticle(ControlCenterRestApi::new, new DeploymentOptions(), whenDeployed);
        return whenDeployed.future().mapEmpty();
    }
}
