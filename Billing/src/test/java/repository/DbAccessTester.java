package repository;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.*;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class DbAccessTester {
    private final String DB_COLLECTION = "test_calculations";
    private MonitoringRepository monitoringRepo;
    private MongoClient mongoClient;

    @Before
    public void setUp() {
//        this.userRepo = new UserRepository(new MongoClientFactory().create());

        Vertx vertx = Vertx.vertx();

        this.mongoClient = new MongoClientFactory().create(vertx, DB_COLLECTION);
        this.monitoringRepo = new MonitoringRepository(this.mongoClient);
    }


    @Test
    public void shouldReadInstanceLoad(TestContext context) {
        this.monitoringRepo.add(MonitoringRepository.InstanceLoad.create(1, 50))
                .compose(objectId -> this.monitoringRepo.add(MonitoringRepository.InstanceLoad.create(2, 70)))
                .compose(objectId -> this.monitoringRepo.add(MonitoringRepository.InstanceLoad.create(3, 20)))
                .compose(lastAdded -> {
                    Future<MonitoringRepository.InstanceLoad> firstRes = this.monitoringRepo.get(1).setHandler(context.asyncAssertSuccess(res -> {
                        context.assertEquals(res.getLoad(), 50);
                    }));

                    Future<MonitoringRepository.InstanceLoad> secondRes = this.monitoringRepo.get(2).setHandler(context.asyncAssertSuccess(res -> {
                        context.assertEquals(res.getLoad(), 70);
                    }));

                    Future<MonitoringRepository.InstanceLoad> thirdRes = this.monitoringRepo.get(3).setHandler(context.asyncAssertSuccess(res -> {
                        context.assertEquals(res.getLoad(), 20);
                    }));

                    return CompositeFuture.all(firstRes, secondRes, thirdRes);
                })
                .compose(checked -> this.monitoringRepo.update(MonitoringRepository.InstanceLoad.create(1, 23))
                            .compose(updated -> this.monitoringRepo.get(1)
                                    .setHandler(context.asyncAssertSuccess(res -> context.assertEquals(res.getLoad(), 23)))
                            )
                )
                .setHandler(context.asyncAssertSuccess());
    }

}
