import io.jsonwebtoken.Jwts;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jwt.JWT;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import jdk.nashorn.internal.objects.annotations.Setter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.bind.DatatypeConverter;
import java.util.stream.IntStream;

@RunWith(VertxUnitRunner.class)
public class JwtTest {

    private JWTAuth provider;

    @Before
    public void setup() {
        Vertx vertx = Vertx.vertx();
        this.provider = JWTAuth.create(vertx, new JWTAuthOptions()
                .addPubSecKey(new PubSecKeyOptions()
                        .setAlgorithm("HS256")
                        .setPublicKey("keyboard cat")
                        .setSymmetric(true)));

        new JWT();
    }

    @Test
    public void generateJwt() {
        IntStream.range(1, 6).forEach(id -> {
            String token = generateToken(id);
            System.out.println("token for account-id: " + id + " " + token);
        });
    }

    private String generateToken(int id) {

        return provider.generateToken(new JsonObject().put("acc-id", id), new JWTOptions());
    }
}
