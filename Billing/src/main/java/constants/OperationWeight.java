package constants;

import lombok.Getter;

public enum OperationWeight {

    ADDITION(1),
    SUBTRACTION(1),
    MULTIPLICATION(2),
    DIVISION(2),
    PRIMES(3),
    FIBONACCI(5);

    @Getter
    private int weight;

    private OperationWeight(int weight) {
        this.weight = weight;
    }


}
