package business;

import constants.OperationWeight;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.ext.mongo.MongoClient;
import repository.MonitoringRepository;
import repository.UsersRepository;

public class BillingService {
    private MonitoringRepository monitoringRepository;
    private UsersRepository usersRepository;

    public BillingService(MongoClient mongoClient){
        this.monitoringRepository = new MonitoringRepository(mongoClient);
        this.usersRepository = new UsersRepository(mongoClient);
    }

    public Future<UsersRepository.User> getUser (int userId){
        return this.usersRepository.get(userId);
    }

    public Future<UsersRepository.User> bill(int userId, int instanceId, OperationWeight operationWeight){
        return this.monitoringRepository.get(instanceId)
                .compose(instanceLoad -> this.deductFromUser(userId, this.calculateBill(instanceLoad, operationWeight)));
    }

    public Future<UsersRepository.User> topup (int userId, double increaseBy) {
        return this.usersRepository.get(userId).compose(user -> {
           user.updateBalance(user.getBalance() + increaseBy);
           return  this.usersRepository.updateUser(user).map(ignored -> user);
        });
    }

    private Future<UsersRepository.User> deductFromUser(int userId, double bill) {
        return this.usersRepository.get(userId).compose(user -> {
            user.updateBalance(user.getBalance() - bill);
            return this.usersRepository.updateUser(user).map(ignored -> user);
        });
    }

    private double calculateBill(MonitoringRepository.InstanceLoad instanceLoad, OperationWeight operationWeight) {
        return instanceLoad.getLoad() * operationWeight.getWeight();
    }

}
