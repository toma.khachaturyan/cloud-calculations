import numpy

from pymongo import MongoClient

# db client credentials
client = MongoClient("192.168.64.61", 30001, maxPoolSize=50)
db = client.calculations


def create_users(y, coll):
    user_id = 1
    for x in range(y):
        rand_balance = numpy.random.uniform(low=50, high=2000, size=1)[0]
        coll.insert_one(
            {
                "id": user_id,
                "balance": rand_balance
            }
        )
        user_id += 1

def insertInstance(instance_id):
    def_load = 0.1
    collection = db.instances
    collection.insert_one(
        {
            "id": instance_id,
            "load": def_load
        }
    )

def main(i):
    collection = db.users
    create_users(i, collection)


if __name__ == "__main__":
    main(100)

