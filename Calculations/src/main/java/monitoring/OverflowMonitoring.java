package monitoring;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.RabbitMQOptions;
import lombok.extern.slf4j.Slf4j;
import repository.MongoClientFactory;
import repository.MonitoringRepository;

import java.util.Objects;
import java.util.Optional;

@Slf4j
public class OverflowMonitoring extends AbstractVerticle {
    private static final int instance = Optional.ofNullable(System.getenv("INSTANCE"))
            .map(String::hashCode)
            .orElse(1);

    public static final String ADDRESS = OverflowMonitoring.class.getName();
    private static final int maxCapacity = 100;
    private static double storage = 0;
    private MonitoringRepository monitoringRepository;


    private RabbitMQClient rabbitMQ;
    private String queue;

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        log.info("Overflow Monitoring Started with instance id {}", instance);

        this.queue = Optional.ofNullable(System.getenv("QUEUE")).orElse(RabbitMQConstants.QUEUE_NAME);

        createMonitoringRepository();
        registerInstance();


        createRabbitMqClient().compose(rabbitStarted -> this.declareExchange())
                .compose(exchangeDeclared -> {
                    log.info("exchanges declared for rabbit mq");
                    this.vertx.eventBus().<JsonObject>localConsumer(ADDRESS).handler(this::handleMessage);
                    return Future.<Void>succeededFuture();
                })
                .setHandler(startPromise);


    }

    private void createMonitoringRepository() {

        MongoClient mongoClient = new MongoClientFactory().create(vertx);
        this.monitoringRepository = new MonitoringRepository(mongoClient);
    }

    private void registerInstance(){
        if(Objects.isNull(this.monitoringRepository)){
            throw new IllegalStateException("mongo db should be started");
        }
        this.monitoringRepository.add(MonitoringRepository.InstanceLoad.create(instance, 0));
    }

    private Future<Void> declareExchange() {

        if (Objects.isNull(this.rabbitMQ)) {
            throw new IllegalStateException("Rabbit mq must be started");
        }

        Promise<Void> whenDeclared = Promise.promise();

        this.rabbitMQ.exchangeDeclare(RabbitMQConstants.EXCHANGE_NAME_OVERLOAD,
                RabbitMQConstants.EXCHANGE_TYPE,
                true,
                false, ar -> {
                    if (ar.succeeded()) {
                        this.rabbitMQ.queueDeclare(RabbitMQConstants.QUEUE_NAME,
                                true,
                                false,
                                false, arQueue -> {
                                    if (arQueue.succeeded()) {
                                        this.rabbitMQ.queueBind(RabbitMQConstants.QUEUE_NAME,
                                                RabbitMQConstants.EXCHANGE_NAME_OVERLOAD,
                                                RabbitMQConstants.ROUTING_KEY_OVERLOAD,
                                                arBind -> {
                                                    if (arBind.succeeded()) {
                                                        whenDeclared.complete();
                                                    } else {
                                                        whenDeclared.fail(arBind.cause());
                                                    }
                                                }
                                        );
                                    } else {
                                        whenDeclared.fail(arQueue.cause());
                                    }
                                });

                    } else {
                        whenDeclared.fail(ar.cause());
                    }
                });

        return whenDeclared.future();
    }

    private void handleMessage(Message<JsonObject> objectMessage) {
        log.info("received this {}", objectMessage.body());
        JsonObject body = objectMessage.body();
        Integer weight = body.getInteger("weight");
        Boolean completed = body.getBoolean("completed");
        storage = completed ? storage - weight : storage + weight;
        this.pushNewOverflow();
    }

    private void pushNewOverflow() {
        JsonObject messageObj = new JsonObject().put(RabbitMQConstants.MESSAGE_BODY, new JsonObject()
                .put("instance", instance)
                .put("overflow", storage / maxCapacity).toString()
        );

        String routingKey = String.format(RabbitMQConstants.ROUTING_KEY_OVERLOAD, RabbitMQConstants.CONSUMER_BILLING);

        this.rabbitMQ.basicPublish(RabbitMQConstants.EXCHANGE_NAME_OVERLOAD, routingKey, messageObj, ar -> {
            if (ar.succeeded()) {
                log.info("published info to queue");
            } else {
                log.error("failed to publish to queue", ar.cause());
            }
        });
    }

    private Future<Void> createRabbitMqClient() {
        Promise<Void> whenStarted = Promise.promise();

        //TODO change the host
        RabbitMQOptions config = new RabbitMQOptions()
                .setUser("guest")
                .setPassword("guest")
                .setHost("rabbitmq.default.svc.cluster.local")
                .setPort(5672)
                .setVirtualHost("/")
                .setConnectionTimeout(6000) // in milliseconds
                .setRequestedHeartbeat(60) // in seconds
                .setHandshakeTimeout(6000) // in milliseconds
                .setRequestedChannelMax(5)
                .setNetworkRecoveryInterval(500) // in milliseconds
                .setAutomaticRecoveryEnabled(true);

        this.rabbitMQ = RabbitMQClient.create(this.vertx, config);
        this.rabbitMQ.start(whenStarted);

        log.info("rabbit mq client created successfully");
        return whenStarted.future();
    }


    private static class RabbitMQConstants {
        static String EXCHANGE_NAME_OVERLOAD = "user";
        static String EXCHANGE_TYPE = "direct";
        static String ROUTING_KEY_OVERLOAD = "overload";
        static String MESSAGE_BODY = "body";
        static String QUEUE_NAME_JSON_KEY = "queue";
        static String QUEUE_NAME = "overload_factor";
        static String EVENT_BUS_ADDRESS = "user.login";
        static String CONSUMER_BILLING = "billing";
    }

}
